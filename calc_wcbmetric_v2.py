import numpy as np
from datetime import datetime
from netCDF4 import Dataset
import windspharm
import argparse
import xarray as xr
import dict_wcbmetric_v2 as dictwcb
from utils import *
import sys

from keras.models import Model
from keras.layers import Input, BatchNormalization, Activation, Dropout
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate
from keras.optimizers import Adam

import pylab
import matplotlib.pyplot as plt

# Function to calculate weighted standard deviation
def weighted_std(values, weights):
    """
    Return the weighted average and standard deviation.
    values, weights -- Numpy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    variance = np.average((values-average)**2, weights=weights)  # Fast and numerically precise
    return math.sqrt(variance)   

def padding_and_normalization(Xtmp, padding, weights):
    """
    Return array that is padded along dateline and normalized
    """
    X = np.zeros([dictwcb.calcdict['ny_cnn'], dictwcb.calcdict['nx_cnn']])
    Xtmp = (Xtmp - np.average(Xtmp, weights=weights))/weighted_std(Xtmp, weights)
    X[:, padding:-padding] = Xtmp[:, :]
    X[:,   -padding:] = Xtmp[:, :padding]
    X[:,    :padding] = Xtmp[:, -padding:]
    return X

def read_ocv(stage, date):
    """
    Read decision thresholds above which modelled
    probabilities are set to 1.
    """
    if date == '0229':
        ocvfile = Dataset('%s/thresholds/ocv_0228' % (dictwcb.calcdict['basedir']), 'r')
    else:
       ocvfile = Dataset('%s/thresholds/ocv_%s' % (dictwcb.calcdict['basedir'], date), 'r')
    ocv   = ocvfile.variables[stage][:, :]
    ocvfile.close()
    return ocv

def read_wcb(stage, date):
    """
    Read 30-d running mean WCB climatology
    """
    ncfile = Dataset('%s/data/hit_%s' % (dictwcb.calcdict['basedir'], date[4:8]), 'r')
    WCB = ncfile.variables[stage][0, 0, :, :-1]
    ncfile.close()
    return WCB

# Funtion to add 2 convolutional layers        
def conv2d_block(input_tensor, n_filters, kernel_size = 3, batchnorm = True):
    """
    Function to add 2 convolutional layers with the parameters passed to it
    """
    # first layer
    x = Conv2D(filters = n_filters, kernel_size = (kernel_size, kernel_size),\
              kernel_initializer = 'he_normal', padding = 'same')(input_tensor)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation('relu')(x)

    # second layer
    x = Conv2D(filters = n_filters, kernel_size = (kernel_size, kernel_size),\
              kernel_initializer = 'he_normal', padding = 'same')(input_tensor)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation('relu')(x)

    return x

def get_unet(input_img, n_filters = 16, dropout = 0.1, batchnorm = True):
    """
    Function to define the UNET Model (Ronneberger et al. 2015)
    """
    # Contracting Path
    c0 = conv2d_block(input_img, n_filters * 1, kernel_size = 3, batchnorm = batchnorm)
    p0 = MaxPooling2D((2, 2))(c0)
    p0 = Dropout(dropout)(p0)

    c1 = conv2d_block(p0, n_filters * 2, kernel_size = 3, batchnorm = batchnorm)
    p1 = MaxPooling2D((2, 2))(c1)
    p1 = Dropout(dropout)(p1)

    c2 = conv2d_block(p1, n_filters * 4, kernel_size = 3, batchnorm = batchnorm)
    p2 = MaxPooling2D((2, 2))(c2)
    p2 = Dropout(dropout)(p2)

    c3 = conv2d_block(p2, n_filters * 8, kernel_size = 3, batchnorm = batchnorm)
    p3 = MaxPooling2D((2, 2))(c3)
    p3 = Dropout(dropout)(p3)

    c4 = conv2d_block(p3, n_filters = n_filters * 16, kernel_size = 3, batchnorm = batchnorm)

    # Expansive path
    u5 = Conv2DTranspose(n_filters * 8, (3, 3), strides = (2, 2), padding = 'same')(c4)
    u5 = concatenate([u5, c3])
    u5 = Dropout(dropout)(u5)
    c5 = conv2d_block(u5, n_filters * 8, kernel_size = 3, batchnorm = batchnorm)

    u6 = Conv2DTranspose(n_filters * 4, (3, 3), strides = (2, 2), padding = 'same')(c5)
    u6 = concatenate([u6, c2])
    u6 = Dropout(dropout)(u6)
    c6 = conv2d_block(u6, n_filters * 4, kernel_size = 3, batchnorm = batchnorm)

    u7 = Conv2DTranspose(n_filters * 2, (3, 3), strides = (2, 2), padding = 'same')(c6)
    u7 = concatenate([u7, c1])
    u7 = Dropout(dropout)(u7)
    c7 = conv2d_block(u7, n_filters * 2, kernel_size = 3, batchnorm = batchnorm)

    u8 = Conv2DTranspose(n_filters * 1, (3, 3), strides = (2, 2), padding = 'same')(c7)
    u8 = concatenate([u8, c0])
    u8 = Dropout(dropout)(u8)
    c8 = conv2d_block(u8, n_filters * 1, kernel_size = 3, batchnorm = batchnorm)

    outputs = Conv2D(1, (1, 1), activation='sigmoid')(c8)
    model = Model(inputs=[input_img], outputs=[outputs])
    return model

def load_model(stage):
    """
    Load model weights and compile model for inflow
    """
    input_img = Input((dictwcb.calcdict['ny_cnn'], dictwcb.calcdict['nx_cnn'], 5), name='img')

    model = get_unet(input_img, n_filters=32, dropout=0.0, batchnorm=True)
    model.load_weights('%s/models/cnn_%s.h5' % (dictwcb.calcdict['basedir'], stage))
    model.compile(optimizer=Adam(), loss="binary_crossentropy", metrics=["accuracy"])
    return model

def calc_GT800(model, Z, U, V, Q, T, PLAG, lon, lat, lev, latmin_index, latmax_index, lonmin_index, lonmax_index):  
    """
    Function to calculate conditional probability of WCB inflow
    """
    X = np.zeros([1, dictwcb.calcdict['ny_cnn'], dictwcb.calcdict['nx_cnn'], 5])
    lon2d, lat2d = np.meshgrid(lon, lat)
    weights = np.cos(np.deg2rad(lat2d))
    
    # Calculate thickness advction
    Zdp = calc_dvardp(Z, 1.e2*lev)
    dZdpdx = calc_dvardx(Zdp, lon, lat)
    dZdpdy = calc_dvardy(Zdp, lon, lat)
    var1 = (dictwcb.calcdict['gt800_multiplier'][0]*(-1.*U*dZdpdx - V*dZdpdy))
    X[0, :, :, 0] = padding_and_normalization((var1[np.where(lev==dictwcb.calcdict['gt800_lev'][0]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate meridional moisture flux
    var2 = dictwcb.calcdict['gt800_multiplier'][1]*V*Q
    X[0, :, :, 1] = padding_and_normalization((var2[np.where(lev==dictwcb.calcdict['gt800_lev'][1]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate moisture flux convergence
    mflx = U*Q; mfly = V*Q
    var3 = dictwcb.calcdict['gt800_multiplier'][2]*calc_div(mflx, mfly, lon, lat)
    X[0, :, :, 2] = padding_and_normalization((var3[np.where(lev==dictwcb.calcdict['gt800_lev'][2]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])

    # Calculate moist potential vorticity
    THE = calc_the(T, Q, lev)
    mpv = calc_PV(U, V, THE, lon, lat, 1.e2*lev)
    mpv[:, lat<0., :] = -1.*mpv[:, lat<0., :]
    var4 = dictwcb.calcdict['gt800_multiplier'][3]*mpv
    X[0, :, :, 3] = padding_and_normalization((var4[np.where(lev==dictwcb.calcdict['gt800_lev'][3]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Conditional probability of WCB ascent with a certain time-lag
    var5 = dictwcb.calcdict['gt800_multiplier'][4]*PLAG
    X[0, :, :, 4] = padding_and_normalization(var5[:, lonmin_index:lonmax_index], \
        dictwcb.calcdict['padding'], weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    p = model.predict_on_batch(X)[0, :, :, 0]
          
    return p[:, dictwcb.calcdict['padding']:-dictwcb.calcdict['padding']]

def calc_MIDTROP(model, Z, U, V, Q, T, CLIM, lon, lat, lev, latmin_index, latmax_index, lonmin_index, lonmax_index):    
    """
    Function to calculate conditional probability of WCB ascent
    """
    X = np.zeros([1, dictwcb.calcdict['ny_cnn'], dictwcb.calcdict['nx_cnn'], 5])
    lon2d, lat2d = np.meshgrid(lon, lat)
    weights = np.cos(np.deg2rad(lat2d))
    
    # Calculate relative vorticity
    var1 = dictwcb.calcdict['midtrop_multiplier'][0]*calc_relvort(U, V, lon, lat)
    X[0, :, :, 0] = padding_and_normalization((var1[np.where(lev==dictwcb.calcdict['midtrop_lev'][0]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate relative humidity
    var2 = dictwcb.calcdict['midtrop_multiplier'][1]*calc_rh(T, Q, lev)
    X[0, :, :, 1] = padding_and_normalization((var2[np.where(lev==dictwcb.calcdict['midtrop_lev'][1]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate thickness advection
    Zdp = calc_dvardp(Z, 1.e2*lev)
    dZdpdx = calc_dvardx(Zdp, lon, lat)
    dZdpdy = calc_dvardy(Zdp, lon, lat)
    var3 = (dictwcb.calcdict['midtrop_multiplier'][2]*(-1.*U*dZdpdx - V*dZdpdy))
    X[0, :, :, 2] = padding_and_normalization((var3[np.where(lev==dictwcb.calcdict['midtrop_lev'][2]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate meridional moisture flux
    var4 = dictwcb.calcdict['midtrop_multiplier'][3]*V*Q
    X[0, :, :, 3] = padding_and_normalization((var4[np.where(lev==dictwcb.calcdict['midtrop_lev'][3]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # 30-day running mean WCB climatology
    var5 = dictwcb.calcdict['midtrop_multiplier'][4]*CLIM
    X[0, :, :, 4] = padding_and_normalization(var5[latmin_index:latmax_index, lonmin_index:lonmax_index], \
        dictwcb.calcdict['padding'], weights[latmin_index:latmax_index, lonmin_index:lonmax_index])

    p = model.predict_on_batch(X)[0, :, :, 0]
    
    return p[:, dictwcb.calcdict['padding']:-dictwcb.calcdict['padding']]

def calc_LT400(model, Z, U, V, Q, T, PLAG, lon, lat, lev, latmin_index, latmax_index, lonmin_index, lonmax_index):
    """
    Function to calculate conditional probability of WCB outflow
    """
    X = np.zeros([1, dictwcb.calcdict['ny_cnn'], dictwcb.calcdict['nx_cnn'], 5])
    lon2d, lat2d = np.meshgrid(lon, lat)
    weights = np.cos(np.deg2rad(lat2d))
    
    # Calculate relative humidity
    var1 = dictwcb.calcdict['lt400_multiplier'][0]*calc_rh(T, Q, lev)
    X[0, :, :, 0] = padding_and_normalization((var1[np.where(lev==dictwcb.calcdict['lt400_lev'][0]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate divergent wind
    uwnd = U[:, ::-1, :]
    vwnd = V[:, ::-1, :]

    uwnd, uwnd_info = windspharm.tools.prep_data(uwnd, 'zyx')
    vwnd, vwnd_info = windspharm.tools.prep_data(vwnd, 'zyx')
    w = windspharm.standard.VectorWind(uwnd, vwnd)

    uchi, vchi = w.irrotationalcomponent()
    uchi = windspharm.tools.recover_data(uchi, uwnd_info)
    vchi = windspharm.tools.recover_data(vchi, uwnd_info)

    var2 = dictwcb.calcdict['lt400_multiplier'][1]*np.sqrt(uchi[:, ::-1, :]**2. + vchi[:, ::-1, :]**2.)
    X[0, :, :, 1] = padding_and_normalization((var2[np.where(lev==dictwcb.calcdict['lt400_lev'][1]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate static stability
    TH = temp_to_theta(T, lev)
    dTHdp = calc_dvardp(TH, 1.e2*lev)
    var3 = np.copy(TH)
    for k in range(0, var3.shape[0]):
        var3[k, :, :] = -1.*(Rd*T[k, :, :])/(1.e2*lev[k]*TH[k, :, :])*dTHdp[k, :, :]
    var3 = dictwcb.calcdict['lt400_multiplier'][2]*var3       
    X[0, :, :, 2] = padding_and_normalization((var3[np.where(lev==dictwcb.calcdict['lt400_lev'][2]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Calculate relative vorticity
    var4 = dictwcb.calcdict['lt400_multiplier'][3]*calc_relvort(U, V, lon, lat)
    X[0, :, :, 3] = padding_and_normalization((var4[np.where(lev==dictwcb.calcdict['lt400_lev'][3]), \
        latmin_index:latmax_index, lonmin_index:lonmax_index])[0,0,:,:], dictwcb.calcdict['padding'], \
        weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    # Conditional probability of WCB ascent with a certain time-lag
    var5 = dictwcb.calcdict['lt400_multiplier'][4]*PLAG
    X[0, :, :, 4] = padding_and_normalization(var5[:, lonmin_index:lonmax_index], \
        dictwcb.calcdict['padding'], weights[latmin_index:latmax_index, lonmin_index:lonmax_index])
    
    p = model.predict_on_batch(X)[0, :, :, 0]
    
    return p[:, dictwcb.calcdict['padding']:-dictwcb.calcdict['padding']]
