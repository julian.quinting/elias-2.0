# EuLerian Identification of ascending AirStreams - ELIAS 2.0
This repository includes code and CNN models to derive conditional probabilities of WCB inflow, ascent, and outflow from data at a comparably low spatial and temporal resolution. The models which are contained in the repository directory ./models/ are referred to as _time-lag models_ in Quinting and Grams (2021). For any questions, error reports etc please contact the corresponding author of the study julian.quinting _at_ kit.edu

> Quinting, J. F. and Grams, C. M.: EuLerian Identification of ascending AirStreams (ELIAS 2.0) in numerical weather prediction and climate models – Part 1: Development of deep learning model, Geosci. Model Dev., 15, 715–730, https://doi.org/10.5194/gmd-15-715-2022, 2022. 

Application examples of the convolutional neural networks are provided in a companion study

>  Quinting, J. F., Grams, C. M., Oertel, A., and Pickl, M.: EuLerian Identification of ascending AirStreams (ELIAS 2.0) in numerical weather prediction and climate models – Part 2: Model application to different datasets, Geosci. Model Dev., 15, 731–744, https://doi.org/10.5194/gmd-15-731-2022, 2022. 

**Important: If you are using ELIAS 2.0 in a publication please include a reference to** 
> Quinting, J. F. and Grams, C. M.: EuLerian Identification of ascending AirStreams (ELIAS 2.0) in numerical weather prediction and climate models – Part 1: Development of deep learning model, Geosci. Model Dev., 15, 715–730, https://doi.org/10.5194/gmd-15-715-2022, 2022.

## Prerequisites
Install the conda environment using `conda env create -f conda_env.yml.` Alternatively, install the following packages in a new conda environment.
```
conda create -n tf tensorflow
conda activate tf
conda install -c conda-forge matplotlib
conda install -c conda-forge netCDF4
conda install -c conda-forge keras
conda install -c anaconda xarray
conda install -c numba numba
conda install -c conda-forge windspharm
conda install -c anaconda scipy
conda install -c conda-forge tensorflow
```

## Data
ELIAS 2.0 is trained on ERA-Interim data at a global latitude-longitude grid of **1.0° grid spacing**. The input data need to be provided on the same grid. Input data are needed at the following **pressure levels**: 1000, 850, 700, 500, 300, 200 hPa. These variables are required: **temperature (T), specific humidity (qv), geopotential (phi), zonal wind (u), and meridional wind (v)**. Based on these variables the following predictors are derived on the fly:
| P | WCB inflow | WCB ascent | WCB outflow |
| ------ | ------ | ------ | ------|
| 1 | 700-hPa thickness advection | 850-hPa relative vorticity | 300-hPa relative humidity |
| 2 | 850-hPa meridional moisture flux | 700-hPa relative humidity | 300-hPa irrotational wind speed |
| 3 | 1000-hPa moisture flux convergence | 300-hPa thickness advection | 500-hPa static stability |
| 4 | 500-hPa moist potential vorticity | 500-hPa meridional moisture flux | 300-hPa relative vorticity |
| 5 | conditional probability of ascent (+24 h)* | 30-d WCB ascent climatology** | conditional probability of ascent (-24 h)* |

\* Calculated on the fly. ** Provided in the ./data/ directory.

## Usage of Jupyter Notebook
An example Notebook is provided to calculate probabilities and masks of WCB inflow, ascent, and outflow for 04 October 2016 from ERA-Interim data. The ERA-Interim data for this case study are provided in the ./data/ directory. The 30-d running mean WCB climatology is needed for WCB ascent and is provided in the same ./data/ directory. The decision thresholds which convert the conditional probabilities to dichotomous predictions are provided in netcdf Format in the ./thresholds/ directory. 
