#!/usr/bin/env python

import numpy as np
import matplotlib 
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.colors as colors
from matplotlib.colors import BoundaryNorm
import scipy as sp
import os, datetime
import math
import calendar
import sys
import cartopy
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import pylab
from pylab import *
from scipy.ndimage.filters import gaussian_filter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        
def plotsetup(extent, fontsize):
    """ Setup of cartopy plot """
    ax = plt.axes(projection=cartopy.crs.PlateCarree())
    ax.coastlines('110m', color='0.4')
    ax.add_feature(cartopy.feature.LAND)
    ax.set_extent(extent)
    ax.set_xticks([-60, -30, 0], crs=cartopy.crs.PlateCarree())
    ax.set_xlim(extent[0],extent[1])
    ax.set_yticks([30, 60], crs=cartopy.crs.PlateCarree())
    ax.tick_params(axis="x", direction='in', pad=-9, labelsize=fontsize)
    ax.tick_params(axis="y", direction='in', pad=-15, labelsize=fontsize)
    gl = ax.gridlines(crs=cartopy.crs.PlateCarree(), draw_labels=False,
                  linewidth=0.5, color='0.5', linestyle='--')
    gl.top_labels = False
    gl.right_labels = False
    gl.xlocator = mticker.FixedLocator(np.linspace(-180.,180.,13))
    gl.ylocator = mticker.FixedLocator(np.linspace(-90.,90.,7))
    gl.xlabel_style = {'size': fontsize}
    gl.ylabel_style = {'size': fontsize}
  
    return ax, gl
        
basepath = './data'

var = 'LT400' # Specify variable to plot ('GT800', 'MIDTROP', 'LT400')
if var == 'GT800':
    labels = ['a','d']
elif var == 'MIDTROP':
    labels = ['b','e']
elif var == 'LT400':
    labels = ['c','f']

ne = 51 # number of ensemble members
fc_dt = 6 # time step of forecast data
t_fc_min = 0 # minimum forecast lead time
t_fc_max = 168 # maximum forecast lead time

# List of fc leadtimes
fcleadtimes = [i for i in range(t_fc_min, t_fc_max+1, fc_dt)]

# List of ensemble members
ensmembers = [str(i).zfill(2) for i in range(1,ne)]
ensmembers.append('control_fc')

# plotting options
extent = [-90, 30, 15, 80]
fontsize=8
    
# Plot Brier Skill Score
infile = np.load('%s/ecmwf_eps_%sbss.npz' % (basepath, var))
lbss = infile['arr_0'] # Brier Skill Score of EPS for WCBs identified with LAGRANTO
ebss = infile['arr_1'] # Brier Skill Score of EPS for WCBs identified with ELIAS
infile.close()

pylab.clf()
fig=pylab.figure(21,figsize=(6,4))
plt.plot(fcleadtimes[:-4], lbss[:-4], label='trajectory approach', color='0.4', ls='--')
plt.plot(fcleadtimes[:-4], ebss[:-4], label='CNN-based approach', color='k')
plt.ylim(0.,1.)
plt.legend(loc='upper right')
plt.xlabel('forecast leadtime [hours]')
plt.ylabel('Brier skill score')
plt.savefig('figure5%s.png' % (labels[0]), dpi=300)
plt.close()

# Plot bias
infile = np.load('%s/ecmwf_eps_%sbias.npz' % (basepath, var))
ebias = infile['arr_0'] # EPS bias identified with ELIAS
lbias = infile['arr_1'] # EPS bias identified with LAGRANTO
eclim = infile['arr_6'] # WCB climatology identified with ELIAS
lclim = infile['arr_7'] # WCB climatology identified with LAGRANTO
pval =  infile['arr_9'] # P-value
infile.close()

# Smooth contour data for visualization purposes
for l, leadtime in enumerate(fcleadtimes):
    eclim[l, :, :] = gaussian_filter(eclim[l, :, :], sigma=1, order=0)
    lclim[l, :, :] = gaussian_filter(lclim[l, :, :], sigma=1, order=0)

# contour fill specification
levels = (-3.,-2.5,-2.,-1.5,-1.,-.5,.5,1.,1.5,2.,2.5,3.)
cmap = plt.get_cmap('RdBu_r')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=False)
plt.rcParams.update({'hatch.color': 'g'})

# plotting
lonplot = np.linspace(-90.,30.,121)
latplot = np.linspace(0.,89.,90)

for l, leadtime in enumerate(fcleadtimes[:-4]):
    if leadtime == 126.:
        emean = 1.e2*ebias[l:l+4, :, :].mean(0)
        lmean = 1.e2*lbias[l:l+4, :, :].mean(0)
        emean[abs(emean) < 0.5] = np.nan
        lmean[abs(lmean) < 0.5] = np.nan
        
        fig=pylab.figure(21,figsize=(7,4))
        ax, gl = plotsetup(extent, fontsize)
        im = ax.pcolormesh(lonplot, latplot, emean, cmap=cmap, norm=norm)
        cbar=fig.colorbar(im, ax=ax, orientation='horizontal', pad=0.05, aspect=40, shrink=0.6)
        cbar.ax.tick_params(labelsize=fontsize)
        cs=plt.contour(lonplot, latplot, 1.e2*eclim[l:l+4, :, :].mean(0), levels=(2.,4.,6.,8.,10.,12.,14.), colors=('k'), linewidths=1)
        plt.clabel(cs, levels=(2.,4.,6.,8.,10.,12.,14.), fontsize=fontsize, inline=True, fmt='%.0f')
        plt.contourf(lonplot, latplot, pval[l:l+4, :, :].mean(0), levels=(-1.e3, 0.1), colors='none', hatches=['...',None])
        plt.savefig('figure4%s.png' % (labels[0]), dpi=300)
        plt.close()

        fig=pylab.figure(21,figsize=(7,4))
        ax, gl = plotsetup(extent, fontsize)
        im = ax.pcolormesh(lonplot, latplot, lmean, cmap=cmap, norm=norm)
        cbar=fig.colorbar(im, ax=ax, orientation='horizontal', pad=0.05, aspect=40, shrink=0.6)
        cbar.ax.tick_params(labelsize=fontsize)
        cs=plt.contour(lonplot, latplot, 1.e2*lclim[l:l+4, :, :].mean(0), levels=(2.,4.,6.,8.,10.,12.,14.), colors=('k'), linewidths=1)
        plt.clabel(cs, levels=(2.,4.,6.,8.,10.,12.,14.), fontsize=fontsize, inline=True, fmt='%.0f')
        plt.contourf(lonplot, latplot, pval[l:l+4, :, :].mean(0), levels=(-1.e3, 0.1), colors='none', hatches=['...',None])
        plt.savefig('figure4%s.png' % (labels[1]), dpi=300)
        plt.close()
