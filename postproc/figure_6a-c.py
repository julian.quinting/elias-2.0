#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use('Agg')
import os
import numpy as np
import pylab
import time
from datetime import date
from netCDF4 import Dataset
from pylab import *
import cartopy
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker

os.environ["CARTOPY_USER_BACKGROUNDS"] = "./cartopy_backgrounds/"

class PiecewiseNorm(Normalize):
  """ Normalization of Color map """
  def __init__(self, levels, clip=False):
        # the input levels
        self._levels = np.sort(levels)
        # corresponding normalized values between 0 and 1
        self._normed = np.linspace(0, 1, len(levels))
        Normalize.__init__(self, None, None, clip)

  def __call__(self, value, clip=None):
        # linearly interpolate to get the normalized value
        return np.ma.masked_array(np.interp(value, self._levels, self._normed))
    
def plotsetup(extent, fontsize):
    """ Setup of cartopy plot """
    ax = plt.axes(projection=cartopy.crs.PlateCarree(central_longitude=0))
    ax.background_img(name='BM', resolution='low')
    ax.set_extent(extent, crs=cartopy.crs.PlateCarree())
    gl = ax.gridlines(crs=cartopy.crs.PlateCarree(), draw_labels=True,
                    linewidth=0.5, color='0.5', linestyle='-')
    gl.top_labels = False
    gl.right_labels = False
    gl.xlocator = mticker.FixedLocator(np.linspace(-180.,180.,37))
    gl.ylocator = mticker.FixedLocator(np.linspace(-80.,80.,17))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fontsize}
    gl.ylabel_style = {'size': fontsize}
    
    return ax, gl

# Define directories according to your needs
inpath = './data'
outpath  = ''

# Specify list of dates and plot headings
datelist = ['20161004_00', '20161004_12', '20161005_00']

# Specify WCB stage to plot at each date
stagelist = ['GT800', 'MIDTROP', 'LT400']

# Define plotting domain
Slat = 32; Nlat = 75
Wlon = -55; Elon = 5

# Map extent
extent = [Wlon, Elon, Slat, Nlat]
# fontsize in plots
fontsize = 10

# Read colortable for SAT IR colorbar
sh_levels = np.linspace(-80., 50., 66)
sh_norm = PiecewiseNorm(sh_levels)

# Loop over dates in datelist
for i, date in enumerate(datelist):
    
    # Read trajectory data initialized from 0 - 48 h before current date
    for hh in range(0, 49, 6):
        validdate = (datetime.datetime.strptime(date, '%Y%m%d_%H')
                     - datetime.timedelta(hours=hh)).strftime('%Y%m%d_%H')
        
        print('Reading trajectory data for %s' % validdate)
        indata = np.genfromtxt('%s/lsl_%s_label' % (inpath, validdate), skip_header=5)
        
        timestep  = abs(indata[0, 0] - indata[1, 0])
        timerange = max(abs(indata[:, 0]))-min(indata[:, 0])
        ncols     = indata.shape[1]
        nsteps    = int((timerange/timestep)+1)
        ntra      = int(indata.shape[0]/(nsteps))
        
        indata = np.reshape(indata, (int(indata.shape[0]/(nsteps)), nsteps, ncols)) 
        time = indata[:, :, 0]
        timeidx = np.argmin(abs(time[0, :]-float(hh)))
        pressure = indata[:, :, 3]
        lat = indata[:, :, 2]
        
        # Determine for each trajectory the WCB stage at the corresponding time
        for j in range(0, ntra):
            if stagelist[i] == 'LT400':
                edgecolor = 'b'
                label = 'c'
                if pressure[j, time[j, :]==float(hh)] <= 400.: # and pressure[j, time[j, :]==float(hh)] > 400.:
                    try:  
                        tradata = np.vstack((tradata, indata[j:j+1, timeidx, :]))
                    except NameError:
                        tradata = indata[j:j+1, timeidx, :]
            if stagelist[i] == 'MIDTROP':
                edgecolor = 'g'
                label = 'b'
                if pressure[j, time[j, :]==float(hh)] > 400. and pressure[j, time[j, :]==float(hh)] <= 800.:
                    try:  
                        tradata = np.vstack((tradata, indata[j:j+1, timeidx, :]))
                    except NameError:
                        tradata = indata[j:j+1, timeidx, :]
            if stagelist[i] == 'GT800':
                edgecolor = 'r'
                label = 'a'
                if pressure[j, time[j, :]==float(hh)] > 800.:
                    try:  
                        tradata = np.vstack((tradata, indata[j:j+1, timeidx, :]))
                    except NameError:
                        tradata = indata[j:j+1, timeidx, :]
    
    # Read conditional WCB probabilities
    print('Reading CNN conditional probabilities data for %s' % date)
    Sfile = Dataset('%s/hit_%s_dt24' % (inpath, date))
    lon = Sfile.variables['lon'][:]
    lat = Sfile.variables['lat'][:]
    lon2d, lat2d = np.meshgrid(lon, lat) 

    Sidx = np.argmin(abs(Slat-lat))
    Nidx = np.argmin(abs(Nlat-lat))
    Widx = np.argmin(abs(Wlon-lon))
    Eidx = np.argmin(abs(Elon-lon))
    
    GT800p = Sfile.variables['GT800p'][Sidx:Nidx, Widx:Eidx]
    MIDTROPp = Sfile.variables['MIDTROPp'][Sidx:Nidx, Widx:Eidx]
    LT400p = Sfile.variables['LT400p'][Sidx:Nidx, Widx:Eidx]
    Sfile.close()  
        
    # Download and read satellite data    
    print('Downloading and reading Sat data')
    satfile="GRIDSAT-B1.%s.%s.%s.%s.v02r01.nc" % (date[:4],date[4:6],date[6:8],date[9:11])
    if os.path.isfile(satfile) == False:  
        os.system("wget --quiet https://www.ncei.noaa.gov/data/geostationary-ir-channel-brightness-temperature-gridsat-b1/access/%s/%s" % (date[:4], satfile))
    Sfile = Dataset('%s' % satfile, 'r')
    latsat = np.array(Sfile.variables['lat'])
    lonsat = np.array(Sfile.variables['lon'])
    
    SAT    = np.array(Sfile.variables['irwin_cdr'][0, :, :])
    SAT    = (200. + SAT*0.01)-273.15
    SAT[SAT > -10.] = np.nan
    lonsat2d, latsat2d = np.meshgrid(lonsat, latsat)
    Sfile.close()

    # Plotting the data
    print('Plotting the data for %s' % (date))
    projection_data = cartopy.crs.PlateCarree()
    fig=pylab.figure(22,figsize=(6,6))
    ax, gl = plotsetup(extent, fontsize)
    im=ax.contourf(lonsat2d, latsat2d, SAT[:, :], levels=sh_levels, norm=sh_norm, cmap = plt.get_cmap('Greys'))
    ax.contour(lon2d[Sidx:Nidx, Widx:Eidx], lat2d[Sidx:Nidx, Widx:Eidx], GT800p, levels=(0.2,0.3,0.4), colors='r', linewidths=1., zorder=5)
    ax.contour(lon2d[Sidx:Nidx, Widx:Eidx], lat2d[Sidx:Nidx, Widx:Eidx], MIDTROPp, levels=(0.2,0.3,0.4), colors='g', linewidths=1., zorder=5)
    ax.contour(lon2d[Sidx:Nidx, Widx:Eidx], lat2d[Sidx:Nidx, Widx:Eidx], LT400p, levels=(0.2,0.3,0.4), colors='b', linewidths=1.5, zorder=5)
    
    sc=ax.scatter(tradata[:, 1], tradata[:, 2], c=tradata[:, 3], s=12, cmap=plt.get_cmap('RdBu'), zorder=1, transform=projection_data, edgecolors=edgecolor, linewidths=0.5)

    axcb = fig.colorbar(sc, orientation='horizontal', pad=0.05, aspect=30, shrink=0.9)
    pylab.savefig( "%s/figure_6%s.png" % (outpath, label),dpi=300)
    os.system("convert -trim %s/figure_6%s.png %s/figure_6%s.png" % (outpath, label, outpath, label))
    plt.close()
    
    del(tradata)
