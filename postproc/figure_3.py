#!/usr/bin/env python

import numpy as np
import matplotlib 
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.colors import BoundaryNorm
import scipy as sp
from scipy import ndimage
from pstats import *
from numpy import trapz
import os, datetime
import math
import calendar
import sys
import cartopy
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import pylab
from pylab import *

# Specify variable to verify ('GT800', 'MIDTROP', 'LT400')
var  = 'MIDTROP'

if var == 'GT800':
    label = 'a'
elif var == 'MIDTROP':
    label = 'b'
elif var == 'GT800':
    label = 'c'

# Specify reanalysis dataset (available are 'ec.erai', 'jra55')
models = ['','JRA55_ERAI_fimportance0', 'JRA55_ERAI_fimportance1', 'JRA55_ERAI_fimportance2', 'JRA55_ERAI_fimportance3', 'JRA55_ERAI_fimportance4','JRA55']
colors = ['k', '#ad494a', 'k', '#ad494a', '#ad494a', '#ad494a', 'k']

# Path of Lagrangian (lwcb) and Eulerian (ewcb) outflow mask data
datapath = './data'
plotpath  = './data'

# plot format
pltfmt = 'pdf'
small_obj_filter = False

# Specify time range (note: Lagrangian data are only available until December 2016)
startyear = 2005
endyear   = 2016

# Specify season for which to verify
seasons = ['DJF']

# Plot figure
fig=pylab.figure(18,figsize=(5,5))
plt.plot([0., 1.],[0., 1.], '-', c='0.6', linewidth=1)
plt.plot([0.1, 1.],[0., 0.9], '-', c='0.6', linewidth=.5)
plt.plot([0., 0.9],[0.1, 1.], '-', c='0.6', linewidth=.5)

for per_name in seasons:
    if per_name == 'JJA':
        styles = [':', '--', '--', '--', '--','--','--']
    else:
        styles = [':', '--', '--', '--', '--','--','--']
    for m, model in enumerate(models):
        if model == '':
            indata = np.loadtxt('%s/RELIABILITY_%s_%s_%i_%i_Filt%s_ec.erai.txt' % (datapath, var, per_name, startyear, endyear, small_obj_filter), delimiter=',', dtype=float)
        else:
            indata = np.loadtxt('%s/%s/RELIABILITY_%s_%s_%i_%i_Filt%s_ec.erai.txt' % (datapath, model, var, per_name, startyear, endyear, small_obj_filter), delimiter=',', dtype=float)

        fc = indata[:, 0] # forecast data
        obs = indata[:, 1] # observation data
        if model == 'ERAI_JRA55_fimportance0' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#ad494a', lw=1., label=r'ERAI & JRA55 $\zeta_{850}$')
        elif model == 'ERAI_JRA55_fimportance1' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#bd9e39', lw=1., label=r'ERAI & JRA55 RH$_{700}$')
        elif model == 'ERAI_JRA55_fimportance2' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#8ca252', lw=1., label=r'ERAI & JRA55 THA$_{300}$')
        elif model == 'ERAI_JRA55_fimportance3' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#6b6ecf', lw=1., label=r'ERAI & JRA55 MFLY$_{500}$')
        elif model == 'ERAI_JRA55_fimportance0' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#ad494a', lw=1., label=r'ERAI & JRA55 THA$_{700}$')
        elif model == 'ERAI_JRA55_fimportance1' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#bd9e39', lw=1., label=r'ERAI & JRA55 MFLY$_{850}$')
        elif model == 'ERAI_JRA55_fimportance2' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#8ca252', lw=1., label=r'ERAI & JRA55 MFLCON$_{1000}$')
        elif model == 'ERAI_JRA55_fimportance3' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#6b6ecf', lw=1., label=r'ERAI & JRA55 MPV$_{500}$')
        elif model == 'ERAI_JRA55_fimportance0' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#ad494a', lw=1., label=r'ERAI & JRA55 RH$_{300}$')
        elif model == 'ERAI_JRA55_fimportance1' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#bd9e39', lw=1., label=r'ERAI & JRA55 wspd$_{\chi 300}$')
        elif model == 'ERAI_JRA55_fimportance2' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#8ca252', lw=1., label=r'ERAI & JRA55 $\sigma_{500}$')
        elif model == 'ERAI_JRA55_fimportance3' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#6b6ecf', lw=1., label=r'ERAI & JRA55 $\zeta_{300}$')
        elif model == 'JRA55_ERAI_fimportance0' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#ad494a', lw=1., label=r'JRA55 & ERAI $\zeta_{850}$')
        elif model == 'JRA55_ERAI_fimportance1' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#bd9e39', lw=1., label=r'JRA55 & ERAI RH$_{700}$')
        elif model == 'JRA55_ERAI_fimportance2' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#8ca252', lw=1., label=r'JRA55 & ERAI THA$_{300}$')
        elif model == 'JRA55_ERAI_fimportance3' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='#6b6ecf', lw=1., label=r'JRA55 & ERAI MFLY$_{500}$')
        elif model == 'JRA55_ERAI_fimportance4' and var == 'MIDTROP':
            plt.plot(fc, obs, linestyle=styles[m], c='b', lw=1., label=r'JRA55 & ERAI WCBCLIM')
        elif model == 'JRA55_ERAI_fimportance0' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#ad494a', lw=1., label=r'JRA55 & ERAI THA$_{700}$')
        elif model == 'JRA55_ERAI_fimportance1' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#bd9e39', lw=1., label=r'JRA55 & ERAI MFLY$_{850}$')
        elif model == 'JRA55_ERAI_fimportance2' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#8ca252', lw=1., label=r'JRA55 & ERAI MFLCON$_{1000}$')
        elif model == 'JRA55_ERAI_fimportance3' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='#6b6ecf', lw=1., label=r'JRA55 & ERAI MPV$_{500}$')
        elif model == 'JRA55_ERAI_fimportance4' and var == 'GT800':
            plt.plot(fc, obs, linestyle=styles[m], c='b', lw=1., label=r'JRA55 & ERAI MIDTROP')
        elif model == 'JRA55_ERAI_fimportance0' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#ad494a', lw=1., label=r'JRA55 & ERAI RH$_{300}$')
        elif model == 'JRA55_ERAI_fimportance1' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#bd9e39', lw=1., label=r'JRA55 & ERAI wspd$_{\chi 300}$')
        elif model == 'JRA55_ERAI_fimportance2' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#8ca252', lw=1., label=r'JRA55 & ERAI $\sigma_{500}$')
        elif model == 'JRA55_ERAI_fimportance3' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='#6b6ecf', lw=1., label=r'JRA55 & ERAI $\zeta_{300}$')
        elif model == 'JRA55_ERAI_fimportance4' and var == 'LT400':
            plt.plot(fc, obs, linestyle=styles[m], c='b', lw=1., label=r'JRA55 & ERAI MIDTROP')
        elif model == 'optimal':
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label='CNN, %s' % per_name)
        elif model == 'JRA55':
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label='JRA55')
        elif model == 'JRA55_RECALIBRATED':
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label='CNN JRA55 RECALIBRATED')
        elif model == 'ec.erai_20y_wcb':
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label='logistic regression, %s' % per_name)
        elif model == 'jra55_wcb':
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label='LR JRA55')
        elif model == 'jra55_wcb_bias':
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label='LR JRA55 RECALIBRATED')
        elif model == '':
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label='ERAI')
        else:  
            plt.plot(fc, obs, linestyle=styles[m], c=colors[m], lw=1., label=model)
    
plt.xlim(0.,1.)
plt.ylim(0.,1.)
plt.legend(frameon=False)
plt.xlabel('Modelled probability')
plt.ylabel('Observed frequency')
plt.savefig('%s/figure_3%s.%s' % (plotpath, label, pltfmt))
