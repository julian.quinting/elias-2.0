#!/usr/bin/env python
'''
This script can be used to reproduce Figures 1-2 of Quinting et al. 2021.
However, the 6-hourly blocking and cyclone data are not publicly available
yet. Please ask the corresponding author for access to the data if needed.
'''

import numpy as np
import matplotlib 
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.colors as colors
from matplotlib.colors import BoundaryNorm
import scipy as sp
from scipy import ndimage
from pstats import *
import os, datetime
import math
from netCDF4 import Dataset
import calendar
from scipy.io import netcdf
import sys
import cartopy
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import pylab
from pylab import *
from scipy.ndimage.filters import gaussian_filter

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

class MidpointNormalize(matplotlib.colors.Normalize):
    def __init__(self, vmin, vmax, midpoint=0, clip=False):
        self.midpoint = midpoint
        matplotlib.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        normalized_min = max(0, 1 / 2 * (1 - abs((self.midpoint - self.vmin) / (self.midpoint - self.vmax))))
        normalized_max = min(1, 1 / 2 * (1 + abs((self.vmax - self.midpoint) / (self.midpoint - self.vmin))))
        normalized_mid = 0.5
        x, y = [self.vmin, self.midpoint, self.vmax], [normalized_min, normalized_mid, normalized_max]
        return sp.ma.masked_array(sp.interp(value, x, y))
    
class PiecewiseNorm(Normalize):
    def __init__(self, levels, clip=False):
        # the input levels
        self._levels = np.sort(levels)
        # corresponding normalized values between 0 and 1
        self._normed = np.linspace(0, 1, len(levels))
        Normalize.__init__(self, None, None, clip)

    def __call__(self, value, clip=None):
        # linearly interpolate to get the normalized value
        return np.ma.masked_array(np.interp(value, self._levels, self._normed))

def daterange(date1, date2):
    for n in range(int ((date2 - date1).days)+1):
        yield date1 + datetime.timedelta(n)
        
def plotsetup(extent, fontsize):
    ax = plt.axes(projection=cartopy.crs.PlateCarree())
    ax.coastlines('110m', color='0.5')
    ax.set_extent(extent)
    ax.set_xticks([-120, -60, 0, 60, 120], crs=cartopy.crs.PlateCarree())
    ax.set_xlim(-180.,180.)
    ax.set_yticks([30, 60], crs=cartopy.crs.PlateCarree())
    ax.tick_params(axis="x", direction='in', pad=-9, labelsize=fontsize)
    ax.tick_params(axis="y", direction='in', pad=-15, labelsize=fontsize)
    gl = ax.gridlines(crs=cartopy.crs.PlateCarree(), draw_labels=False,
                    linewidth=0.5, color='0.5', linestyle='--')
    gl.top_labels = False
    gl.right_labels = False
    gl.xlocator = mticker.FixedLocator(np.linspace(-180.,180.,7))
    gl.ylocator = mticker.FixedLocator(np.linspace(-90.,90.,7))
    gl.xlabel_style = {'size': fontsize}
    gl.ylabel_style = {'size': fontsize}
    
    return ax, gl

# Specify variable to verify
# Set var to 'MIDTROP' for figure 1 and to 'LT400' for figure 2
var  = 'LT400'
exp = '_4layer_MIDTROP'
filters = '_32filters' 

# Delete small objects
small_obj_filter = 'False'
size = 4.e4 # 4.e4 seems to be a good choice for outflow

# Specify reanalysis dataset (available are 'ec.erai', 'jra55')
model = 'ec.erai'

# Specify verification score that is wanted
scores = ['cyc_match','block_match']

# Optimal cut-off values ('f1', 'hss', 'const')
ocvmetric = 'bias'

# Path of Lagrangian (lwcb) and Eulerian (ewcb) outflow mask data
lwcbpath  = ''
ewcbpath  = ''
plotpath  = '.'

# Set specifications for plots
# Map extent
extent = [-180, 180, 0, 89]
# Contour levels for Lagrangian WCB climatology
clevs  = (2.,4.,6.,8.,10.)
# fontsize in plots
fontsize = 8
# plot format
pltfmt = 'pdf'
# write verification score to file
write = True

# Specify time range (note: Lagrangian data are only available until December 2016)
startyear = 2005
endyear   = 2016

# Specify season for which to verify
per_name  = 'DJF'

# Create list of dates for specified time range
datelist = []
for yyyy in range(startyear, endyear):
    if per_name == 'DJF':
        if (yyyy+1)%4 == 0:
            startdate = datetime.date(yyyy,  12,  1)
            enddate = datetime.date(yyyy+1,  2, 29)
        else:
            startdate = datetime.date(yyyy,  12,  1)
            enddate = datetime.date(yyyy+1,  2, 28)
    else: 
        print('Period not defined:', per_name)
        break
    
    for dt in daterange(startdate, enddate):
        interimdate = dt.strftime("%Y%m%d")
        for hh in ['00', '06', '12', '18']:
            datelist.append('%s_%s' % (interimdate, hh))

# Loop pver all dates
for i, interimdate in enumerate(datelist):
    print(interimdate)
    
    # Read Eulerian WCB data  
    ncfile = Dataset('%s/2020_CNN/update/cnn%s%s_year_1980_2000_00UTC_12UTC/%s/%s/hit_%s' % (ewcbpath, exp, filters, interimdate[:4], interimdate[4:6], interimdate), 'r')
    if i == 0:
        lon = ncfile.variables['lon'][:]
        lat = ncfile.variables['lat'][:]
        
        # determine index boundaries of box
        lonWidx = np.argmin(abs(lon-extent[0])); lonEidx = np.argmin(abs(lon-extent[1]))+1
        latSidx = np.argmin(abs(lat-extent[2])); latNidx = np.argmin(abs(lat-extent[3]))+1

        lon = lon[lonWidx:lonEidx]; lat = lat[latSidx:latNidx]    
        lon2d, lat2d = np.meshgrid(lon, lat)

        nx, ny = lon.shape[0], lat.shape[0]
        LWCB = np.zeros([len(datelist), ny, nx])
        EWCB = np.zeros([len(datelist), ny, nx])
        EWCBF = np.zeros([len(datelist), ny, nx])
        if 'cyc_match' in scores:
            LCYC = np.zeros([len(datelist), ny, nx])
            ECYC = np.zeros([len(datelist), ny, nx])
        if 'block_match' in scores:
            LBLOCK = np.zeros([len(datelist), ny, nx])
            EBLOCK = np.zeros([len(datelist), ny, nx])      
            CLIMBLOCK = np.zeros([len(datelist), ny, nx])    

    EWCB[i, :, :] = ncfile.variables['%sp' % var][latSidx:latNidx, lonWidx:lonEidx]
    # Read threshold data to convert conditional probabilities to binary masks
    if ocvmetric == 'bias':
        if interimdate[4:8] == '0229':
            ocvfile = Dataset('../thresholds/ocv_0228', 'r')
        else:
            ocvfile = Dataset('../thresholds/ocv_%s' % (interimdate[4:8]), 'r')
        ocv   = ocvfile.variables[var][6:, :]
        ocvfile.close()

        EWCBF[i, (EWCB[i, :, :]  > ocv)] = 1.
    ncfile.close() 
  
    # Read Lagrangian WCB data
    if os.path.isfile('%s/clim/wcb/grid/%s/%s/hit_%s' % (lwcbpath, interimdate[:4], interimdate[4:6], interimdate)):
        ncfile = Dataset('%s/clim/wcb/grid/%s/%s/hit_%s' % (lwcbpath, interimdate[:4], interimdate[4:6], interimdate), 'r')
        LWCB[i, :, :] = ncfile.variables['%s' % var][0, 0, 90:-1, lonWidx:lonEidx]
        ncfile.close()
    else:
        print('File for %s not found.' % interimdate)
  
    # Read cyclone flags if cyc_match in scores
    if 'cyc_match' in scores:
        ncfile = Dataset('%s/clim/mincl/filter.default/%s/%s/T%s.flag01' % (lwcbpath, interimdate[:4], interimdate[4:6], interimdate), 'r')
        CYC = ncfile.variables['FLAG'][0, 90:-1, lonWidx:lonEidx]
        ncfile.close()
        
        labeled_array, num_features = ndimage.label(EWCBF[i, :, :])
        for y in range(labeled_array.shape[0]):
            if labeled_array[y, 0] > 0 and labeled_array[y, -1] > 0:
                labeled_array[labeled_array == labeled_array[y, -1]] = labeled_array[y, 0]
        for j in range(1, num_features+1):
            if CYC[labeled_array==j].sum() > 0.:
                ECYC[i, labeled_array==j] = 1.
            
        labeled_array, num_features = ndimage.label(LWCB[i, :, :])
        for y in range(labeled_array.shape[0]):
            if labeled_array[y, 0] > 0 and labeled_array[y, -1] > 0:
                labeled_array[labeled_array == labeled_array[y, -1]] = labeled_array[y, 0]
        for j in range(1, num_features+1):
            if CYC[labeled_array==j].sum() > 0.:
                LCYC[i, labeled_array==j] = 1.
        
    if 'block_match' in scores:
        if interimdate[:4] != datelist[i-1][:4]:
            ncfile = Dataset('%s/clim/blocks/output/extended/BLOCKS%s.nc' % (lwcbpath, interimdate[:4]), 'r')
            BLOCK = ncfile.variables['FLAG'][:, 90:-1, lonWidx:lonEidx]
            BLtime = ncfile.variables['time'][:] # get values
            timelist = []
            refdat = datetime.datetime(1950,1,1,0)
            for h in range(0, BLtime.shape[0]):
                timelist.append(datetime.datetime.strftime(refdat+datetime.timedelta(hours=int(BLtime[h])), '%Y%m%d_%H'))
            ncfile.close()
        
        timeindex = timelist.index(interimdate)
        
        CLIMBLOCK[i, :, :] = BLOCK[timeindex, :, :]
        EBLOCK[i, (BLOCK[timeindex, :, :] == 1.) & (EWCBF[i, :, :] == 1.)] = 1.
        LBLOCK[i, (BLOCK[timeindex, :, :] == 1.) & (LWCB[i, :, :] == 1.)] = 1.
    
# Now perform verification
for score in scores:
    if score == 'cyc_match':
        # contour fill specification
        levels = np.linspace(-30.,30.,13)
        cmap = plt.get_cmap('RdBu_r')
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=False)
        
        ECYCm = 1.e2*ECYC.mean(0)
        EWCBm = 1.e2*EWCBF.mean(0)
        LCYCm = 1.e2*LCYC.mean(0)
        LWCBm = 1.e2*LWCB.mean(0)
        
        freq = 1.e2*LCYCm/LWCBm
        freq = ndimage.gaussian_filter(freq, sigma=1, order=0)
        freq[LWCBm < 1.] = np.nan
        freqe = 1.e2*ECYCm/EWCBm
        freqe[LWCBm < 1.] = np.nan
        diff = 1.e2*(ECYCm/EWCBm-LCYCm/LWCBm)
        diff[LWCBm < 1.] = np.nan
        
        # plotting
        fig=pylab.figure(21,figsize=(7,4))
        ax, gl = plotsetup(extent, fontsize)
        im = ax.pcolormesh(lon, lat, diff, cmap=cmap, norm=norm)
        cbar=fig.colorbar(im, ax=ax, orientation='horizontal', pad=0.05, aspect=40)
        cbar.ax.tick_params(labelsize=fontsize)
        cs=plt.contour(lon, lat, freq, levels=(10.,20.,30.,40.,50.,60.,70.,80.,90.), colors=('k'), linewidths=0.5)
        plt.clabel(cs, levels=(10.,20.,30.,40.,50.,60.,70.,80.,90.), fontsize=fontsize-3, inline=True, fmt='%.0f')
        plt.savefig('%s/figure_1b.%s' % (plotpath, pltfmt))
        os.system('pdfcrop %s/figure_1b.%s %s/figure_1b.%s' % (plotpath, pltfmt, plotpath, pltfmt))
        plt.close()
        
        levels = np.linspace(60.,100.,9)
        cmap = plt.get_cmap('inferno_r')
        cmap = truncate_colormap(cmap, 0.,.8)
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        # plotting
        fig=pylab.figure(21,figsize=(7,4))
        ax, gl = plotsetup(extent, fontsize)
        im = ax.pcolormesh(lon, lat, freqe, cmap=cmap, norm=norm)
        cbar=fig.colorbar(im, ax=ax, orientation='horizontal', pad=0.05, aspect=40)
        cbar.ax.tick_params(labelsize=fontsize)
        cs=plt.contour(lon, lat, freq, levels=(10.,20.,30.,40.,50.,60.,70.,80.,90.), colors=('k'), linewidths=0.5)
        plt.clabel(cs, levels=(10.,20.,30.,40.,50.,60.,70.,80.,90.), fontsize=fontsize-3, inline=True, fmt='%.0f')
        plt.savefig('%s/figure_1a.%s' % (plotpath, pltfmt))
        os.system('pdfcrop %s/figure_1a.%s %s/figure_1a.%s' % (plotpath, pltfmt, plotpath, pltfmt))
        plt.close()
        
    if score == 'block_match':
        # contour fill specification
        levels = np.linspace(-15.,15.,11)
        cmap = plt.get_cmap('RdBu_r')
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=False)
        
        clim = CLIMBLOCK.mean(0)
        freq = LBLOCK.mean(0)/CLIMBLOCK.mean(0)
        freq = ndimage.gaussian_filter(freq, sigma=1, order=0)
        freqe = EBLOCK.mean(0)/CLIMBLOCK.mean(0)
        diff = 1.e2*(EBLOCK.mean(0)/CLIMBLOCK.mean(0)-LBLOCK.mean(0)/CLIMBLOCK.mean(0))
        diff[clim<0.01]=np.nan
        freq[clim<0.01]=np.nan
        freqe[clim<0.01]=np.nan
        
        # plotting
        fig=pylab.figure(21,figsize=(7,4))
        ax, gl = plotsetup(extent, fontsize)
        im = ax.pcolormesh(lon, lat, diff, cmap=cmap, norm=norm)
        cbar=fig.colorbar(im, ax=ax, orientation='horizontal', pad=0.05, aspect=40)
        cbar.ax.tick_params(labelsize=fontsize)
        cs=plt.contour(lon, lat, 1.e2*(freq), levels=(5.,10.,15.,20.,25.,30.,35.,40.,45.), colors=('k'), linewidths=0.5)
        plt.clabel(cs, levels=(5.,10.,15.,20.,25.,30.,35.,40.,45.), fontsize=fontsize-3, inline=True, fmt='%.0f')
        plt.savefig('%s/figure_2b.%s' % (plotpath, pltfmt))
        os.system('pdfcrop %s/figure_2b.%s %s/figure_2b.%s' % (plotpath, pltfmt, plotpath, pltfmt))
        plt.close()
        
        levels = np.linspace(0.,55.,12)
        cmap = plt.get_cmap('inferno_r')
        cmap = truncate_colormap(cmap, 0.,.8)
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=False)
        fig=pylab.figure(21,figsize=(7,4))
        ax, gl = plotsetup(extent, fontsize)
        im = ax.pcolormesh(lon, lat, 1.e2*freqe, cmap=cmap, norm=norm)
        im.cmap.set_under('w')
        im.set_clim(levels[1])
        cbar=fig.colorbar(im, ax=ax, orientation='horizontal', pad=0.05, aspect=40)
        cbar.ax.tick_params(labelsize=fontsize)
        cs=plt.contour(lon, lat, 1.e2*(freq), levels=(5.,10.,15.,20.,25.,30.,35.,40.,45.), colors=('k'), linewidths=0.5)
        plt.clabel(cs, levels=(5.,10.,15.,20.,25.,30.,35.,40.,45.), fontsize=fontsize-3, inline=True, fmt='%.0f')
        plt.savefig('%s/figure_2a.%s' % (plotpath, pltfmt))
        os.system('pdfcrop %s/figure_2a.%s %s/figure_2a.%s' % (plotpath, pltfmt, plotpath, pltfmt))
        plt.close()
      
