"""
The ICON source code is distributed under an institutional license issued by the German Weather Service (DWD). For more information see https://code.mpimet.mpg.de/projects/iconpublic.
The model output of the ICON simulation is available from the authors upon request.
"""

"""
Plotting script for Fig. 6 d-f in Quinting et al. (submitted)
"""

# import modules
import numpy as np
import netCDF4
from netCDF4 import Dataset
from datetime import datetime
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap 
import matplotlib as mpl
import os
import os.path
import sys

import jq_modules
from utilities_01 import setup_global_map

# import colormaps
from utilities_01 import make_cmap
cmapPVN, normPVN, levelsPVN = make_cmap()['PVN']

# create colormaps for plotting
cmap_dp = plt.cm.RdYlBu #mpl.cm.summer
bounds_dp = np.arange(-400,-10,10) #ctopp levels
norm_dp = mpl.colors.BoundaryNorm(bounds_dp,240)

cmap2 = plt.cm.RdYlBu #mpl.cm.summer
bounds2 = np.arange(-300,100,25) #ctopp levels
norm2 = mpl.colors.BoundaryNorm(bounds2,cmap2.N)

# read ICON variables
def load_ICON_vars(var_list, nc_file):
    d = {}
    for vv in var_list:
        print(vv)
        d[vv] = np.squeeze(nc_file.variables[vv])
    return d

# plotting setup
ll_lims = [-55,32]
ur_lims = [5,75]
dlon = 10
dlat = 10

# read ICON output file after REMAPPING (directly performed in simulation)
date=datetime(2016,10,4,12)
path2NN = local_path + "/ICON_runs/dt_IFSIC_ref_conv_111_FTT_ddt_gspc5/icon_output_dt_IFSIC_ref_conv_111_FTT_ddt_gscp5/"
path2file = local_path + "/ICON_runs/dt_IFSIC_ref_conv_111_FTT_ddt_gspc5/icon_output_dt_IFSIC_ref_conv_111_FTT_ddt_gscp5/"

# load constants file
# specify domain
DOM="DOM02"
constants_filename = path2file + "ICONCONST_" + DOM + "_" + DOM + "_ML_0001.nc" 
constants_nc = netCDF4.Dataset(constants_filename) 
HHL = np.squeeze(constants_nc.variables["z_ifc"]) # half level height
HFL = np.squeeze(constants_nc.variables["z_mc"]) # full level height
topo = np.squeeze(constants_nc.variables["topography_c"])
topo[HFL[0,:,:]==0] = np.nan # remove grid points which are out of domain
lons_c = np.squeeze(constants_nc.variables["lon"])
lats_c = np.squeeze(constants_nc.variables["lat"])

# set plotting flags
plt_dt6 = True
plt_dt12 = True

# WCB trajectory directory
wcbdir = path2file + "/trajs_DOM02/"
year = 2016
month=10

##########################################
#INFLOW
##########################################
date=datetime(2016,10,4,0)
#%% 
# load processed WCB trajectory data
filename_dp = wcbdir + "dp_tpm1_all_WCB_" + date.strftime("%Y%m%d_%H.npy")
if os.path.isfile(filename_dp) == False:   
    print("cannot find " + filename_dp)
elif os.path.isfile(filename_dp) == True:
    print("read in " + filename_dp)
    dp_tpm1_ar = np.load(filename_dp)


#%%
# read in netcdfs of WCB metric 2.0 output (with dt=24h lag)
NN_filename = path2NN + "hit_2.1_" + date.strftime("%Y%m%d_%H")
nn_nc = netCDF4.Dataset(NN_filename)
lon_1d_nn = np.squeeze(nn_nc.variables["lon"])
lat_1d_nn = np.squeeze(nn_nc.variables["lat"])
lons_nn,lats_nn = np.meshgrid(lon_1d_nn, lat_1d_nn)
midtropo = np.squeeze(nn_nc.variables["MIDTROPp"])
GT800 = np.squeeze(nn_nc.variables["GT800p"])
LT400 = np.squeeze(nn_nc.variables["LT400p"])

# Add "standard" metric 2.0 without ascent as predictor
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_" + date.strftime("%Y%m%d_%H")
nn0_nc = netCDF4.Dataset(NN_filename)
midtropo_0 = np.squeeze(nn0_nc.variables["MIDTROPp"])
GT800_0 = np.squeeze(nn0_nc.variables["GT800p"])
LT400_0 = np.squeeze(nn0_nc.variables["LT400p"])

# Add "lagged" metric 2.0 with dt=6h for inflow and outflow lag
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_dt6h_2.1_" + date.strftime("%Y%m%d_%H")
nn6_nc = netCDF4.Dataset(NN_filename)
midtropo_6 = np.squeeze(nn6_nc.variables["MIDTROPp"])
GT800_6 = np.squeeze(nn6_nc.variables["GT800p"])
LT400_6 = np.squeeze(nn6_nc.variables["LT400p"])

# Add "lagged" metric 2.0 with dt=12h for inflow and outflow lag
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_dt12h_2.1_" + date.strftime("%Y%m%d_%H")
nn12_nc = netCDF4.Dataset(NN_filename)
midtropo_12 = np.squeeze(nn12_nc.variables["MIDTROPp"])
GT800_12 = np.squeeze(nn12_nc.variables["GT800p"])
LT400_12 = np.squeeze(nn12_nc.variables["LT400p"])

# setup map
setup_global_map(ll_lims[0],ll_lims[1],ur_lims[0],ur_lims[1], dlon,dlat)
xx,yy = dp_tpm1_ar[:,1],dp_tpm1_ar[:,2]
dp_tpm1 = dp_tpm1_ar[:,4] # centered 2-h pressure change of WCB air parcels

plt.scatter(xx[dp_tpm1_ar[:,3] >= 800],yy[dp_tpm1_ar[:,3] >= 800], color="0.8") # WCB air parcels with p>800 hPa               
plt.scatter(xx[dp_tpm1_ar[:,12] <= 6],yy[dp_tpm1_ar[:,12] <= 6], color="0.5") # WCB air parcels 6 h before p<800
plt.scatter(xx[dp_tpm1_ar[:,12] <= 1],yy[dp_tpm1_ar[:,12] <= 1], c=cmap2(norm2(dp_tpm1[dp_tpm1_ar[:,12] <= 1])),s=45) # WCB air parcels 1 h before p<800

plt.contour(lons_nn, lats_nn, midtropo, levels=[0.2,0.3,0.4], colors="green", zorder=98, linewidths=1)
plt.contour(lons_nn, lats_nn, GT800, levels=[0.2,0.3,0.4], colors="red", zorder=99, linewidths=2)
plt.contour(lons_nn, lats_nn, LT400, levels=[0.2,0.3,0.4], colors="blue", zorder=97, linewidths=1)

plt.rcParams.update({'font.size': 18})
plt.contourf(lons_nn,lats_nn,midtropo*1e15, cmap=cmap2, norm=norm2, levels=bounds2)
plt.colorbar(label=r"$\Delta$ p$_{2h}$ [hPa]", orientation="horizontal",fraction=0.05,pad=0.07)
plt.rcParams.update({'font.size': 10})

plt.tight_layout()
#plt.savefig("metric2.1_inflow_residence_dt800_" + DOM + "_" + date.strftime("%Y%m%d_%H%M.png"),bbox_inches="tight", pad_inches = 0.1)


##########################################
#ASCENT
##########################################
date=datetime(2016,10,4,12)

#%% 
# load processed WCB trajectory data
filename_dp = wcbdir + "dp_tpm1_all_WCB_" + date.strftime("%Y%m%d_%H.npy")
if os.path.isfile(filename_dp) == False:   
    print("cannot find " + filename_dp)
elif os.path.isfile(filename_dp) == True:
    print("read in " + filename_dp)
    dp_tpm1_ar = np.load(filename_dp)

#%%
# read in netcdfs of WCB metric 2.0 output (with dt=24h lag)
NN_filename = path2NN + "hit_2.1_" + date.strftime("%Y%m%d_%H")
nn_nc = netCDF4.Dataset(NN_filename)
lon_1d_nn = np.squeeze(nn_nc.variables["lon"])
lat_1d_nn = np.squeeze(nn_nc.variables["lat"])
lons_nn,lats_nn = np.meshgrid(lon_1d_nn, lat_1d_nn)
midtropo = np.squeeze(nn_nc.variables["MIDTROPp"])
GT800 = np.squeeze(nn_nc.variables["GT800p"])
LT400 = np.squeeze(nn_nc.variables["LT400p"])

# Add "standard" metric 2.0 without ascent as predictor
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_" + date.strftime("%Y%m%d_%H")
nn0_nc = netCDF4.Dataset(NN_filename)
midtropo_0 = np.squeeze(nn0_nc.variables["MIDTROPp"])
GT800_0 = np.squeeze(nn0_nc.variables["GT800p"])
LT400_0 = np.squeeze(nn0_nc.variables["LT400p"])

# Add "lagged" metric 2.0 with dt=6h for inflow and outflow lag
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_dt6h_2.1_" + date.strftime("%Y%m%d_%H")
nn6_nc = netCDF4.Dataset(NN_filename)
midtropo_6 = np.squeeze(nn6_nc.variables["MIDTROPp"])
GT800_6 = np.squeeze(nn6_nc.variables["GT800p"])
LT400_6 = np.squeeze(nn6_nc.variables["LT400p"])

# Add "lagged" metric 2.0 with dt=12h for inflow and outflow lag
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_dt12h_2.1_" + date.strftime("%Y%m%d_%H")
nn12_nc = netCDF4.Dataset(NN_filename)
midtropo_12 = np.squeeze(nn12_nc.variables["MIDTROPp"])
GT800_12 = np.squeeze(nn12_nc.variables["GT800p"])
LT400_12 = np.squeeze(nn12_nc.variables["LT400p"])

# setup map
setup_global_map(ll_lims[0],ll_lims[1],ur_lims[0],ur_lims[1], dlon,dlat)
xx,yy = dp_tpm1_ar[:,1],dp_tpm1_ar[:,2]
dp_tpm1 = dp_tpm1_ar[:,4] # ! 

plt.contour(lons_nn, lats_nn, midtropo, levels=[0.2,0.3,0.4], colors="green", zorder=99, linewidths=2)
plt.contour(lons_nn, lats_nn, GT800, levels=[0.2,0.3,0.4], colors="red", zorder=97, linewidths=1)
plt.contour(lons_nn, lats_nn, LT400, levels=[0.2,0.3,0.4], colors="blue", zorder=96, linewidths=1)

# scatter plot of 2h WCB ascent
xx,yy = dp_tpm1_ar[:,1],dp_tpm1_ar[:,2]
dp_tpm1 = dp_tpm1_ar[:,4] # centered 2-h pressure change of WCB air parcels
dp_tpm1[np.where(dp_tpm1_ar[:,9]==0)] = np.nan # set data to np.nan if not within main ascent phase (MAP)

# select the scatter according to ascent
plt.scatter(xx[dp_tpm1<-25],yy[dp_tpm1<-25], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-25])), s=25,zorder=9)
plt.scatter(xx[dp_tpm1<-50],yy[dp_tpm1<-50], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-50])), s=25,zorder=10)
plt.scatter(xx[dp_tpm1<-100],yy[dp_tpm1<-100], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-100])), s=25,zorder=11)
plt.scatter(xx[dp_tpm1<-150],yy[dp_tpm1<-150], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-150])), s=25,zorder=12)
plt.scatter(xx[dp_tpm1<-200],yy[dp_tpm1<-200], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-200])), s=25,zorder=13)
plt.scatter(xx[dp_tpm1<-250],yy[dp_tpm1<-250], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-250])), s=25,zorder=14)
plt.scatter(xx[dp_tpm1<-300],yy[dp_tpm1<-300], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-300])), s=25,zorder=15)
plt.scatter(xx[dp_tpm1<-400],yy[dp_tpm1<-400], c=cmap_dp(norm_dp(dp_tpm1[dp_tpm1<-400])), s=25,zorder=16)
# save file and close

plt.rcParams.update({'font.size': 18})
plt.contourf(lons_nn,lats_nn,midtropo*1e15, cmap=cmap_dp, norm=norm_dp, levels=bounds_dp)
plt.colorbar(label=r"$\Delta$ p$_{2h}$ [hPa]", orientation="horizontal",fraction=0.05,pad=0.07)
plt.rcParams.update({'font.size': 10})

plt.tight_layout()
#plt.savefig("metric2.1_outflow_residence_midtropo_" + DOM + "_" + date.strftime("%Y%m%d_%H%M.png"),bbox_inches="tight", pad_inches = 0.1)


##########################################
#OUTFLOW
##########################################
date=datetime(2016,10,5,0)

#%% 
# load processed WCB trajectory data
filename_dp = wcbdir + "dp_tpm1_all_WCB_" + date.strftime("%Y%m%d_%H.npy")
if os.path.isfile(filename_dp) == False:   
    print("cannot find " + filename_dp)
elif os.path.isfile(filename_dp) == True:
    print("read in " + filename_dp)
    dp_tpm1_ar = np.load(filename_dp)


#%%
# read in netcdfs of WCB metric 2.0 output (with dt=24h lag)
NN_filename = path2NN + "hit_2.1_" + date.strftime("%Y%m%d_%H")
nn_nc = netCDF4.Dataset(NN_filename)
lon_1d_nn = np.squeeze(nn_nc.variables["lon"])
lat_1d_nn = np.squeeze(nn_nc.variables["lat"])
lons_nn,lats_nn = np.meshgrid(lon_1d_nn, lat_1d_nn)
midtropo = np.squeeze(nn_nc.variables["MIDTROPp"])
GT800 = np.squeeze(nn_nc.variables["GT800p"])
LT400 = np.squeeze(nn_nc.variables["LT400p"])

# Add "standard" metric 2.0 without ascent as predictor
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_" + date.strftime("%Y%m%d_%H")
nn0_nc = netCDF4.Dataset(NN_filename)
midtropo_0 = np.squeeze(nn0_nc.variables["MIDTROPp"])
GT800_0 = np.squeeze(nn0_nc.variables["GT800p"])
LT400_0 = np.squeeze(nn0_nc.variables["LT400p"])

# Add "lagged" metric 2.0 with dt=6h for inflow and outflow lag
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_dt6h_2.1_" + date.strftime("%Y%m%d_%H")
nn6_nc = netCDF4.Dataset(NN_filename)
midtropo_6 = np.squeeze(nn6_nc.variables["MIDTROPp"])
GT800_6 = np.squeeze(nn6_nc.variables["GT800p"])
LT400_6 = np.squeeze(nn6_nc.variables["LT400p"])

# Add "lagged" metric 2.1 with dt=12h for inflow and outflow lag
# read in netcdfs of WCB metric 2.0 output
NN_filename = path2NN + "hit_dt12h_2.1_" + date.strftime("%Y%m%d_%H")
nn12_nc = netCDF4.Dataset(NN_filename)
midtropo_12 = np.squeeze(nn12_nc.variables["MIDTROPp"])
GT800_12 = np.squeeze(nn12_nc.variables["GT800p"])
LT400_12 = np.squeeze(nn12_nc.variables["LT400p"])

# setup map
setup_global_map(ll_lims[0],ll_lims[1],ur_lims[0],ur_lims[1], dlon,dlat)
xx,yy = dp_tpm1_ar[:,1],dp_tpm1_ar[:,2]
dp_tpm1 = dp_tpm1_ar[:,4] # centered 2-h pressure change of WCB air parcels

plt.scatter(xx[dp_tpm1_ar[:,3] <= 400],yy[dp_tpm1_ar[:,3] <= 400], color="0.8") # WCB air parcels with p<400 hPa              
plt.scatter(xx[dp_tpm1_ar[:,11] <= 6],yy[dp_tpm1_ar[:,11] <= 6], color="0.5") # WCB air parcels 6 h before p<400
plt.scatter(xx[dp_tpm1_ar[:,11] == 0],yy[dp_tpm1_ar[:,11] == 0], c=cmap2(norm2(dp_tpm1[dp_tpm1_ar[:,11] == 0])),s=45) # WCB air parcels when they transition to p<400

plt.contour(lons_nn, lats_nn, midtropo, levels=[0.2,0.3,0.4], colors="green", zorder=98, linewidths=1)
plt.contour(lons_nn, lats_nn, GT800, levels=[0.2,0.3,0.4], colors="red", zorder=97, linewidths=1)
plt.contour(lons_nn, lats_nn, LT400, levels=[0.2,0.3,0.4], colors="blue", zorder=99, linewidths=2)

# "standard" model 2.0 metric
plt.contour(lons_nn, lats_nn, LT400_0, levels=[0.2,0.3,0.4], colors="darkblue", zorder=98, linewidths=1, linestyles="dashed")

plt.rcParams.update({'font.size': 18})
plt.contourf(lons_nn,lats_nn,midtropo*1e15, cmap=cmap2, norm=norm2, levels=bounds2)
plt.colorbar(label=r"$\Delta$ p$_{2h}$ [hPa]", orientation="horizontal",fraction=0.05,pad=0.07)
plt.rcParams.update({'font.size': 10})

plt.tight_layout()
#plt.savefig("metric2.1_outflow_residence_dt400_" + DOM + "_" + date.strftime("%Y%m%d_%H%M.png"),bbox_inches="tight", pad_inches = 0.1)
