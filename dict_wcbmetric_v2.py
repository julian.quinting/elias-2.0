#!/usr/bin/env python

calcdict = dict(
    basedir = '.',

    all_levs = [1000.,850.,700.,500.,300.],
    
    # Specify vertical levels in hPa
    gt800_lev = [700., 850., 1000., 500., 1000.],
    midtrop_lev = [850., 700., 300., 500., 1000.],
    lt400_lev = [300., 300., 500., 300., 1000.],
    
    # Specify constant multipliers
    gt800_multiplier = [1.e8, 1., -1.e8, 1.e6, 1.],
    midtrop_multiplier = [1., 1., 1.e8, 1., 1.],
    lt400_multiplier = [1., 1., 1., 1., 1.],
    
    # Specify time-lag for inflow and outflow
    gt800_timelag = 24,
    lt400_timelag = -24,
    
    # Specify dimensions of input "images" and padding along date line
    ny_cnn = 96,
    nx_cnn = 448,
    padding = 44,
    
    # Specify latitudes and longitudes of training domain
    training_S = -6.,
    training_N = 89.,
    training_W = -180.,
    training_E = 179.
)
