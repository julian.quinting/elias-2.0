#!/usr/bin/env python

import numpy as np
from scipy import ndimage
from pstats import *
from numpy import trapz
import os, datetime
import math

Cp = 1004.5;
Cv = 717.5;
Rd = 287.04;
Rv = 461.6;
c  = (Rv/Rd-1);
RvRd = Rv / Rd;
g = 9.80665;
L = 2.50e6;
Lf = 3.34*10**5;
Talt = 288.1500;
Tfrez = 273.1500;
To = 300.;
Po = 101325.;
Pr = 1000.;
lapsesta = 6.5 / 1000.;
kappa = Rd / Cp;
epsil = Rd/Rv;
pi = math.pi;
pid = pi/180.;
R_earth = 6371200.;
omeg_e = (2.*pi) / (24.*3600.);
eo = 6.112;
missval = -9999.;
eps = 2.2204e-16

def temp_to_theta(temp, pres):
    ''' Compute potential temperature '''
    ''' '''
    ''' theta: Input potential temperature (K) '''
    ''' pres:  Input pressure (hPa)'''
    ''' temp:  Output temperature (K)'''
    th = np.zeros([temp.shape[0], temp.shape[1], temp.shape[2]])
    for k in range(0, temp.shape[0]):
        th[k, :, :] = temp[k, :, :]* (1000. / pres[k]) ** kappa
    return th

def theta_to_temp(theta, pres):
    ''' Compute temperature '''
    ''' '''
    ''' temp:  Input temperature (K)'''
    ''' pres:  Input pressure (hPa)'''
    ''' theta: Output potential temperature (K)'''
    return theta * (pres / 1000.) ** kappa

def temp_to_tv(temp, qv):
    ''' Compute virtual temperature '''
    ''' '''
    ''' temp: Input temperature '''
    ''' qv: Input specific humidity '''
    ''' Output: virtual temperature '''
    return (1.+c*qv)*temp

def calc_thw(temp,qv,p):
    ''' Compute wet-bulb potential temperature'''
    the = calc_the(temp,qv,p)
    thw = 45.114 - 51.489*(the/Tfrez)**(-1./kappa)
    return thw+Tfrez

def calc_es(temp):
    ''' Compute saturated vapor pressure for -30C<T<35C '''
    ''' temp: Input temperature '''
    ''' Output: saturated vapor pressure in hPa '''
    return eo*np.exp(17.67*(temp-Tfrez)/(temp-Tfrez+243.5))

def calc_mr(qv):
    ''' Compute humidity mixing ratio '''
    ''' Input: specific humidity '''
    ''' Output: mixing ratio in kg/kg '''
    return qv/(1.-qv)

def calc_mrs(temp, p):
    ''' Compute saturated mixing ratio for -30C<T<35C '''
    ''' temp: Input temperature in K '''
    ''' p: Input pressure in hPa '''
    ''' mrs: Output saturated mixing ratio in kg/kg '''
    mrs = np.zeros([temp.shape[0], temp.shape[1], temp.shape[2]])
    for k in range(0, temp.shape[0]):
        mrs[k, :, :] = epsil*(calc_es(temp[k, :, :])/p[k])
    return mrs

def calc_rh(temp, qv, p):
    ''' Compute relative humidity '''
    ''' temp: Input temperature in K '''
    ''' qv: Input specific humidity '''
    ''' p: Input pressure in Pa '''
    rh = 1.e2*calc_mr(qv)/calc_mrs(temp, p)
    return rh

def calc_the(temp, qv, p):
    ''' Compute equivalent potential temperature in K after Bolton (1980, MWR)'''
    ''' temp: Input temperature in K '''
    ''' qv: Input specific humidity '''
    ''' p: Input pressure in hPa '''  
    r = calc_mr(qv)

    thexact = np.zeros([temp.shape[0], temp.shape[1], temp.shape[2]])
    for k in range(0, temp.shape[0]):
        thexact[k, :, :] = temp[k, :, :]*((1000./p[k])**(kappa*(1.-0.28*r[k, :, :])))

    tl = 1./(1./(temp-55.) - np.log(calc_rh(temp, qv, p)/100.)/2840.) + 55.
    the = thexact*np.exp((3.376/tl - 0.00254)*1.e3*r*(1.+0.81*r))
    return the

def calc_thes(temp, qv, p):
    ''' Compute saturated equivalent potential temperature in K after Bolton (1980, MWR)'''
    ''' temp: Input temperature in K '''
    ''' qv: Input specific humidity '''
    ''' p: Input pressure in hPa ''' 
    r = calc_mr(qv)
    rs = calc_mrs(temp, p)

    thexact = np.zeros([temp.shape[0], temp.shape[1], temp.shape[2]])
    for k in range(0, temp.shape[0]):
        thexact[k, :, :] = temp[k, :, :]*((1000./p[k])**(kappa*(1.-0.28*r[k, :, :])))

    thes = thexact*np.exp((3.376/temp - 0.00254)*1.e3*rs*(1.+0.81*rs))
    return thes

def calc_mfl(qv, u, v):
    ''' Compute moisture transport'''
    ''' qv: Input specific humidity '''
    ''' u: Input zonal wind '''
    ''' v: Input meridional wind '''
    mfl = np.sqrt((u*qv)**2. + (v*qv)**2.)
    return mfl

def calc_relvort(u, v, lon, lat):
    ''' Compute relative vorticity on pressure levels '''
    ''' '''
    ''' u: Input zonal wind in m/s '''
    ''' v: Input meridional wind in m/s '''
    ''' lat/lon: 1d-array in degree N/S'''
    lon2d, lat2d = np.meshgrid(lon, lat)
    a_d2r = R_earth*pid 

    # Calculate hor distance in m
    dx = np.zeros([lat.shape[0], lon.shape[0]])
    dx[:,    0] = a_d2r*(lon2d[:,  1] - lon2d[:,   0])*np.cos(lat2d[:, 0]*pid)
    dx[:,   -1] = a_d2r*(lon2d[:, -1] - lon2d[:,  -2])*np.cos(lat2d[:,-1]*pid)
    dx[:, 1:-1] = a_d2r*(lon2d[:, 2:] - lon2d[:, :-2])*np.cos(lat2d[:, 1:-1]*pid)/2.

    # Calculate hor distance in m
    dy = np.zeros([lat.shape[0], lon.shape[0]])
    dy[   0, :] = a_d2r*(lat2d[ 1, :] - lat2d[  0, :])
    dy[  -1, :] = a_d2r*(lat2d[-1, :] - lat2d[ -2, :])
    dy[1:-1, :] = a_d2r*(lat2d[2:, :] - lat2d[:-2, :])/2.

    # Calculate derivatives of v in x direction
    dvdx = np.zeros([v.shape[0], v.shape[1], v.shape[2]])
    for k in range(0, v.shape[0]):
         dvdx[k, :,    0] = (v[k, :,  1] - v[k, :,   0])/dx[ :,  0]
         dvdx[k, :,   -1] = (v[k, :, -1] - v[k, :,  -2])/dx[ :, -1]
         dvdx[k, :, 1:-1] = (v[k, :, 2:] - v[k, :, :-2])/(2.*dx[:, 1:-1])
    
    # Calculate derivatives of u in y direction
    dudy = np.zeros([u.shape[0], u.shape[1], u.shape[2]])
    for k in range(0, u.shape[0]):
         dudy[k,    0, :] = (u[k, 1, :]*np.cos(lat2d[1, :]*pid) \
                            -u[k, 0, :]*np.cos(lat2d[0, :]*pid))/(dy[0, :]*np.cos(lat2d[0, :]*pid))
         dudy[k,   -1, :] = (u[k, -1, :]*np.cos(lat2d[-1, :]*pid) \
                            -u[k, -2, :]*np.cos(lat2d[-2, :]*pid))/(dy[-1, :]*np.cos(lat2d[-1, :]*pid))
         dudy[k, 1:-1, :] = (u[k, 2:, :]*np.cos(lat2d[2:, :]*pid) \
                            -u[k, :-2, :]*np.cos(lat2d[:-2, :]*pid))/(2.*dy[1:-1, :]*np.cos(lat2d[1:-1, :]*pid))
    return dvdx - dudy

def calc_div(u, v, lon, lat):
    ''' Compute divergence on pressure levels '''
    ''' '''
    ''' u: Input zonal wind in m/s '''
    ''' v: Input meridional wind in m/s '''
    ''' lat/lon: 1d-array in degree N/S'''
    lon2d, lat2d = np.meshgrid(lon, lat)
    a_d2r = R_earth*pid

    # Calculate hor distance in m
    dx = np.zeros([lat.shape[0], lon.shape[0]])
    dx[:,    0] = a_d2r*(lon2d[:,  1] - lon2d[:,   0])*np.cos(lat2d[:, 0]*pid)
    dx[:,   -1] = a_d2r*(lon2d[:, -1] - lon2d[:,  -2])*np.cos(lat2d[:,-1]*pid)
    dx[:, 1:-1] = a_d2r*(lon2d[:, 2:] - lon2d[:, :-2])*np.cos(lat2d[:, 1:-1]*pid)/2.
    
    # Calculate hor distance in m
    dy = np.zeros([lat.shape[0], lon.shape[0]])
    dy[   0, :] = a_d2r*(lat2d[ 1, :] - lat2d[  0, :])
    dy[  -1, :] = a_d2r*(lat2d[-1, :] - lat2d[ -2, :])
    dy[1:-1, :] = a_d2r*(lat2d[2:, :] - lat2d[:-2, :])/2.
    
    # Calculate derivatives of u in x direction
    dudx = np.zeros([u.shape[0], u.shape[1], u.shape[2]])
    for k in range(0, u.shape[0]):
         dudx[k, :,    0] = (u[k, :,  1] - u[k, :,   0])/dx[ :,  0]
         dudx[k, :,   -1] = (u[k, :, -1] - u[k, :,  -2])/dx[ :, -1]
         dudx[k, :, 1:-1] = (u[k, :, 2:] - u[k, :, :-2])/(2.*dx[:, 1:-1])

    # Calculate derivatives of v in y direction
    dvdy = np.zeros([v.shape[0], v.shape[1], v.shape[2]])
    for k in range(0, v.shape[0]):
         dvdy[k,    0, :] = (v[k, 1, :]*np.cos(lat2d[1, :]*pid) \
                            -v[k, 0, :]*np.cos(lat2d[0, :]*pid))/(dy[0, :])
         dvdy[k,   -1, :] = (v[k, -1, :]*np.cos(lat2d[-1, :]*pid) \
                            -v[k, -2, :]*np.cos(lat2d[-2, :]*pid))/(dy[-1, :])
         dvdy[k, 1:-1, :] = (v[k, 2:, :]*np.cos(lat2d[2:, :]*pid) \
                            -v[k, :-2, :]*np.cos(lat2d[:-2, :]*pid))/(2.*dy[1:-1, :])
    return dudx+dvdy

def calc_dx(lon, lat):
    ''' lon: 1d-array in degree W/E'''
    lon2d, lat2d = np.meshgrid(lon, lat)
    a_d2r = R_earth*pid
    dx = np.zeros([lat.shape[0], lon.shape[0]])
    dx[:,    0] = a_d2r*(lon2d[:,  1] - lon2d[:,   0])*np.cos(lat2d[:, 0]*pid)
    dx[:,   -1] = a_d2r*(lon2d[:, -1] - lon2d[:,  -2])*np.cos(lat2d[:,-1]*pid)
    dx[:, 1:-1] = a_d2r*(lon2d[:, 2:] - lon2d[:, :-2])*np.cos(lat2d[:, 1:-1]*pid)/2.
    return dx

def calc_dy(lon, lat):
    ''' lat/lon: 1d-array in degree N/S'''
    a_d2r = R_earth*pid
    lon2d, lat2d = np.meshgrid(lon, lat)

    # Calculate hor distance in m
    dy = np.zeros([lat.shape[0], lon.shape[0]])
    dy[   0, :] = a_d2r*(lat2d[ 1, :] - lat2d[  0, :])
    dy[  -1, :] = a_d2r*(lat2d[-1, :] - lat2d[ -2, :])
    dy[1:-1, :] = a_d2r*(lat2d[2:, :] - lat2d[:-2, :])/2.
    return dy

def calc_dvardx(var, lon, lat):
    ''' Compute derivative in x direction '''
    ''' '''
    ''' var: Arbitrary input variable '''
    ''' lon: 1d-array in degree W/E'''
    lon2d, lat2d = np.meshgrid(lon, lat)
    a_d2r = R_earth*pid

    # Calculate hor distance in m
    dx = np.zeros([lat.shape[0], lon.shape[0]])
    dx[:,    0] = a_d2r*(lon2d[:,  1] - lon2d[:,   0])*np.cos(lat2d[:, 0]*pid)
    dx[:,   -1] = a_d2r*(lon2d[:, -1] - lon2d[:,  -2])*np.cos(lat2d[:,-1]*pid)
    dx[:, 1:-1] = a_d2r*(lon2d[:, 2:] - lon2d[:, :-2])*np.cos(lat2d[:, 1:-1]*pid)/2.

    # Calculate derivatives of var in x direction
    dvardx = np.zeros([var.shape[0], var.shape[1], var.shape[2]])
    for k in range(0, var.shape[0]):
         dvardx[k, :,    0] = (var[k, :,  1] - var[k, :,   0])/dx[ :,  0]
         dvardx[k, :,   -1] = (var[k, :, -1] - var[k, :,  -2])/dx[ :, -1]
         dvardx[k, :, 1:-1] = (var[k, :, 2:] - var[k, :, :-2])/(2.*dx[:, 1:-1])
    return dvardx

def calc_dvardy(var, lon, lat):
    ''' Compute derivative in y direction '''
    ''' '''
    ''' var: Arbitrary input variable'''
    ''' lat/lon: 1d-array in degree N/S'''
    a_d2r = R_earth*pid
    lon2d, lat2d = np.meshgrid(lon, lat)

    # Calculate hor distance in m
    dy = np.zeros([lat.shape[0], lon.shape[0]])
    dy[   0, :] = a_d2r*(lat2d[ 1, :] - lat2d[  0, :])
    dy[  -1, :] = a_d2r*(lat2d[-1, :] - lat2d[ -2, :])
    dy[1:-1, :] = a_d2r*(lat2d[2:, :] - lat2d[:-2, :])/2.

    # Calculate derivatives of psi in y direction
    dvardy = np.zeros([var.shape[0], var.shape[1], var.shape[2]])
    for k in range(0, var.shape[0]):
         dvardy[k,   0, :] = (var[k,  1, :] - var[k,   0, :])/dy[ 0, :]
         dvardy[k,  -1, :] = (var[k, -1, :] - var[k,  -2, :])/dy[-1, :]
         dvardy[k,1:-1, :] = (var[k, 2:, :] - var[k, :-2, :])/(2.*dy[1:-1, :])
    return dvardy

def calc_dvardp(var, p):
    ''' Compute vertical derivative '''
    ''' '''
    ''' var: Arbitrary input variable '''
    ''' p: pressure in Pa '''

    # Calculate vertical distance in Pa
    dp = np.zeros([var.shape[0]])
    dp[   0] = (p[ 1] - p[  0])
    dp[  -1] = (p[-1] - p[ -2])
    dp[1:-1] = (p[2:] - p[:-2])/2.

    # Calculate vertical derivative of var
    dvardp = np.zeros([var.shape[0], var.shape[1], var.shape[2]])
    dvardp[   0, :, :] = (var[ 1, :, :] - var[  0, :, :])/dp[ 0]
    dvardp[  -1, :, :] = (var[-1, :, :] - var[ -2, :, :])/dp[-1]
    for k in range(1, var.shape[0]-1):
        dvardp[k, :, :] = (var[k+1, :, :] - var[k-1, :, :])/(2.*dp[k])

    return dvardp
    
def calc_PV(u, v, th, lon, lat, p):
    ''' Compute potential vorticity '''
    ''' on pressure levels (eq 13 Hoskins 1985) '''
    ''' u, v, th: 3D input of zonal wind, meridional wind '''
    ''' and potential temperature '''
    ''' lat/lon: 1d-array in degree N/S'''
    ''' p: pressure in Pa '''

    lon2d, lat2d = np.meshgrid(lon, lat)
    coriolis = 2.*omeg_e*np.sin(lat2d*pid)

    # Calculate derivatives in x direction
    dthdx = calc_dvardx(th, lon, lat)
    dvdx  = calc_dvardx(v , lon, lat)

    # Calculate derivatives in y direction
    dthdy = calc_dvardy(th, lon, lat)
    dudy  = calc_dvardy(u , lon, lat)

    # Calculate derivatives in vertical direction
    dthdp = calc_dvardp(th, p)
    dudp  = calc_dvardp(u , p)
    dvdp  = calc_dvardp(v , p)

    # Calculate absolute vorticity
    abs_vorticity = np.zeros([u.shape[0], u.shape[1], u.shape[2]])
    for k in range(0, u.shape[0]):
        abs_vorticity[k, :, :] = coriolis + dvdx[k, :, :] - dudy[k, :, :]

    # Calculate potential vorticity
    pv = -g*(-dvdp*dthdx + dudp*dthdy + abs_vorticity*dthdp)
    
    return pv

